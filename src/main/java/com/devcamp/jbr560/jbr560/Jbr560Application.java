package com.devcamp.jbr560.jbr560;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr560Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr560Application.class, args);
	}

}
