package com.devcamp.jbr560.jbr560.service;

import java.util.ArrayList;
import java.util.Date;

import com.devcamp.jbr560.jbr560.model.Visit;

public class VisitService {
    Visit visit1 = new Visit(new CustomerService().customer1, new Date());
    Visit visit2 = new Visit(new CustomerService().customer2, new Date());
    Visit visit3 = new Visit(new CustomerService().customer3, new Date());

    public ArrayList<Visit> getAllVisit() {
        ArrayList<Visit> listVisit = new ArrayList<>();
        listVisit.add(visit1);
        listVisit.add(visit2);
        listVisit.add(visit3);
        return listVisit;
    }
}
