package com.devcamp.jbr560.jbr560.service;

import java.util.ArrayList;

import com.devcamp.jbr560.jbr560.model.Customer;

public class CustomerService {
    Customer customer1 = new Customer("QuanNM");
    Customer customer2 = new Customer("BoiHB");
    Customer customer3 = new Customer("ThuNHM");

    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> listCustomer = new ArrayList<>();
        listCustomer.add(customer1);
        listCustomer.add(customer2);
        listCustomer.add(customer3);
        return listCustomer;
    }
}
