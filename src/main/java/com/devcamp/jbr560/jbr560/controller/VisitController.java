package com.devcamp.jbr560.jbr560.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr560.jbr560.model.Visit;
import com.devcamp.jbr560.jbr560.service.VisitService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class VisitController {
    @GetMapping("/visits")
    public ArrayList<Visit> getVisits() {
        return new VisitService().getAllVisit();
    }
}
